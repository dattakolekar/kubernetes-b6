# Kubernetes

## What is Kubernetes?
    
Kubernnetes are orchestration tool (manage vm) it is a open source and create by google to create a application that are easy manage and deploy anywhere.
   
## Why we use kubernetes?
   
Kubermet make it easy to scale applicaton up or down as needed.Kubernets can be used to deploy application on variety of platforms, including on permission, in the cloud, and in hybrid environment.kubernets can help insure that application are always available.

## Features of kubernetes?

- Auto healing
- autoscaling 
- laod balancer
- cluster
- Networking and routing 
- deployment strategies.

## Kubernetes Architecture?

In kubernetes architecture there are two types of nodes
1.Master Node(Control plane)
2.Slave Node (Worker node)
1.MASTER NODE :
- The master is new going to run set of k8s processes wil ensure smooth functioning of cluster these  process are called control plane.
-Master node can be multi-master for high avaibility.
-Master runs control plane to run clusster smoothly.
Components of control plane [master node]
1.Kube-api server
2.Schedular
3.Controller manager
4.ETCD

   a) Kube-apiserver :-This api-server interacts with directly with user (i.e. we apply .yaml or json manifest to kube-apiserver). this api-server is meant to scale automatically as per load. Kube api-server is front-end of control plane.
   b) Scheduler :-When users make request for the creation and management of pods, scheduler is going to take acton on there requests. Handles pod creation and management. Scheduler match/ assign any node to crete and run pods.A scheduler watches for newly created pods that have no nodes assigned for any pod that the scheduler discovers , the scheduler becomes responsible for finding best nodes for that pods to run on.
   c) Controller Manager:- It is make sure that the actual state of cluster matches to desired state.
      Components of masster that runsn controller:
        Node Controller
        Route Controller
        Service Controller
        Volume Controller
        d)ETCD :-Etcd store metadata and store of cluster. etcd consist and high-available store.source of touch for state (info about state of cluster)
2.SLAVE NODE :
   - Slave Node is goining to run the three important pieces of software
   1.Kubelet -Agent running on the node. It is listen to kubernetes master node(pod creation request).use port 10255.send succesfull report to master node.
   2.Container engine -It works with kubelet and pulling the images. it start and stops the containers. The exposing containers on ports spotified in mainfest file.
   3.Kube-proxy  -TO assign ip to the each pods. It is requested to aassign ip address to pods. Kube-proxy runs on each nodes and this.


## Pod Lifecycle

POD :- Pod is smallest entity in kubernetes. entrapment around the container.Pod is group of one or more contanier that are deployed together on the same host.In kubernetes the cintrol unit is pod not container. Consist of one or more tightly coupled container. pod runs on node which is control by master. K8s only knews about PODS. cannot start container without pod.

## Objects:
- Pod - Smallest unit of kubernetes. Its just wrapping around the container
- Service - Expose application 
    - ClusterIP - Expose application inside the cluster
    - NodePort - Expose application outside the cluster
    - LoadBalancer - Expose application outside the cluster 
- Namespace -  To segregate the resources / k8s objects